// create a new scene named "Game"
let gameScene = new Phaser.Scene('Game');

var curve;
var graphics;
//test function
gameScene.lines = function(){
  var lineA = new Phaser.Geom.Line(95, 51, 299, 51);
  var lineB = new Phaser.Geom.Line(301, 51, 505, 51);
  var lineC = new Phaser.Geom.Line(95, 243, 299, 243);
  var lineD = new Phaser.Geom.Line(301, 243, 505, 243);
  var lineD = new Phaser.Geom.Line(301, 243, 505, 243);
  var lineE = new Phaser.Geom.Line(93, 51, 93, 243);
  var lineF = new Phaser.Geom.Line(507, 51, 507, 243);
  
  var graphics = this.add.graphics(0,0);
  
  graphics.lineStyle(4,0xFA8072,1);
  
  graphics.strokeLineShape(lineA);
  graphics.strokeLineShape(lineB);
  graphics.strokeLineShape(lineC);
  graphics.strokeLineShape(lineD);
  graphics.strokeLineShape(lineE);
  graphics.strokeLineShape(lineF);
}

gameScene.curves = function(){
  curve = new Phaser.Curves.Ellipse(100, 100, 100, 150);

  curve.draw(graphics, 64);

  curve.setXRadius(100);
  curve.setYRadius(200);
  curve.setStartAngle(0);
  curve.setEndAngle(180);
  curve.setRotation(0);

}

// some parameters for our scene
gameScene.init = function() {

}

// load asset files for our game
gameScene.preload = function() {
  this.load.image('background', 'assets/court.png');
};

// executed once, after assets were loaded
gameScene.create = function() {
  //this.background = this.add.sprite(300, 147, 'background');
  //this.lines();
  this.curves();
};

// our game's configuration
let config = {
  type: Phaser.AUTO,
  width: 600,
  height: 294,
  scene: gameScene
};

// create the game, and pass it the configuration
let game = new Phaser.Game(config);